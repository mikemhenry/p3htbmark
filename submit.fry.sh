#!/bin/bash
#SBATCH --partition=batch
#SBATCH --job-name=test
#SBATCH --output=res_%j.txt
#
#SBATCH --time=10:00:00
#SBATCH --ntasks=1
#SBATCH --ntasks-per-node=1
#SBATCH --gres gpu:1

module load singularity
cd /home/mikehenry/p3htbmark
singularity exec --nv mikemhenry-cme-lab-images-master-hoomd.simg python -u test.py 
