#!/bin/bash
#
#SBATCH --job-name=test
#SBATCH --output=res_%j.txt
#
#SBATCH --time=10:00:00
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1
#SBATCH --gres gpu:1

module load singularity/2.5.2

singularity exec --nv /cstor/xsede/users/xs-mhenry/.singularity/mikemhenry-cme-lab-images-master-hoomd.simg python test.py
