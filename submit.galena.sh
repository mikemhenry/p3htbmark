#!/bin/bash
#PBS -N test_run_1                   
#PBS -l select=1:ncpus=1:ngpus=1
#PBS -l walltime=01:00:00
#PBS -v DOCKER_IMAGE="nvcr.io/nvidia/pytorch:18.01-py3"
#PBS -P iuc
#PBS -M mikehenry@boisestate.edu
#PBS -e err.txt
#PBS -o out.txt

export PATH=/home/henrmich/cmake-3.12.1/bin:/opt/conda/bin:/bin:/usr/bin:$PATH
export PYTHONPATH=/home/henrmich/hoomd-build/lib/python:$PYTHONPATH

cd /home/henrmich/p3htbmark
python -u test.py
