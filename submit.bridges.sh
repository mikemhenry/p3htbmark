#!/bin/bash
#SBATCH --partition=GPU-shared
#SBATCH --job-name=bmakr
#SBATCH --output=res_%j.txt
#
#SBATCH --time=05:00:00
#SBATCH --ntasks=1
#SBATCH --ntasks-per-node=1
#SBATCH --gres gpu:p100:1

module load singularity
cd /home/mhenry/p3htbmark
singularity exec --nv ../mikemhenry-cme-lab-images-master-hoomd.simg python -u test.py
