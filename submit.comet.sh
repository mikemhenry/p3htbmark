#!/bin/bash
#SBATCH --partition=gpu-shared
#SBATCH --job-name=test
#SBATCH --output=res_%j.txt
#
#SBATCH --time=10:00:00
#SBATCH --ntasks=1
#SBATCH --ntasks-per-node=6
#SBATCH --gres gpu:p100:1

module load singularity
cd /home/mhenry/p3htbmark
singularity exec --nv /home/mhenry/mikemhenry-cme-lab-images-master-hoomd.simg python -u test.py 
